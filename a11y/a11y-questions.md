# A11y Questions
- **Who benefits from accessibility?**
Although there is no template for disability, there are many people who can benefit from accessible technology: People with visual impairments, including color blindness, low-vision, and blindness. People with auditory impairments, including deafness and diminished hearing.
___
- **Name some ways responsive/mobile first design can affect accessibility**
Very opinionated question here is a resource: [What Does Responsive Web Design Have to do with Accessibility?]
(https://www.levelaccess.com/what-does-responsive-web-design-have-to-do-with-accessibility/___
- **What assistive technologies (ATs) are you familiar with (desktop + mobile)?**
  -   **Screen readers:** Software used by blind or visually impaired people to read the content of the computer screen. Examples include JAWS for Windows, NVDA, or Voiceover for Mac.
  -   **Screen magnification software:** Allow users to control the size of text and or graphics on the screen. Unlike using a zoom feature, these applications allow the user to have the ability to see the enlarged text in relation to the rest of the screen. This is done by emulating a handheld magnifier over the screen.
  -   **Text readers:** Software used by people with various forms of learning disabilities that affect their ability to read text. This software will read text with a synthesized voice and may have a highlighter to emphasize the word being spoken. These applications do not read things such as menus or types of elements - they only read the text.
  -   **Speech input software:** Provides people with difficulty in typing an alternate way to type text and also control the computer. Users can give the system some limited commands to perform mouse actions. Users can tell the system to click a link or a button or use a menu item. Examples would be Dragon Naturally Speaking for Windows or Mac. Please note both Windows and Mac have some speech recognition utilities, but they cannot be used to browse the web.
  -   **Alternative input devices:** Some users may not be able to use a mouse or keyboard to work on a computer. These people can use various forms of devices, such as:
      -   **Head pointers:** A stick or object mounted directly on the user’s head that can be used to push keys on the keyboard. This device is used by individuals who have no use of their hands.
      -   **Motion tracking or eye tracking:** This can include devices that watch a target or even the eyes of the user to interpret where the user wants to place the mouse pointer and moves it for the user.
      -   **Single switch entry devices:** These kinds of devices can be used with other alternative input devices or by themselves. These are typically used with on-screen keyboards. The on-screen keyboard has a cursor move across the keys, and when the key the user wants is in focus, the user will click the switch. This can also work on a webpage: the cursor can move through the webpage, and if the user wants a to click on a link or button when that link or button is in focus, the user can activate the switch.

___
- **Describe the purpose of heading and header elements, and how they are useful in websites and web applications.**
Headings are ranked `<h1>` through `<h6>`. Use headings hierarchically (link is external), with the `<h1>` representing the most important idea on the page, and sub-sections organized with `<h2>` level headings. Those sub-sections can themselves be divided with `<h3>` level headings, and so on.

It is best to plan out a heading structure before composing a page. Doing so will help you both select appropriate heading levels and keep your thoughts organized overall.

All pages should at least have a `<h1>` level heading giving the title of the page.

Do not skip heading levels to be more specific (for example, do not skip from `<h2>` to `<h5>`). It is permissible to skip headings in the other direction if the outline of the page calls for it (for example, from `<h5>` to `<h2>`).

Relatedly, do not select heading levels based on their appearance. Select the appropriate heading rank in your hierarchy.

Do not use bold instead of a heading. One of the most common accessibility mistakes is making text bold when a heading is needed. Though the text may look like a heading, the underlying code is not set correctly, and screen reader users will not benefit.

Do not overuse headings. In most cases, content editors will not need more than `<h2>` rank headings and the occasional `<h3>` rank. Only for exceptionally long or complex pages would `<h5>` and `<h6>` be necessary.

**Benefits of Headings**

Organizing web pages by headings helps users get a sense of the page’s organization and structure. Visually, headings are presented as larger and more distinct than surrounding text. Making texts larger helps guide the eye around the page. Using headings and making them visually apparent is especially helpful for users with cognitive disabilities.

If the underlying code for a pages headings is correct, screen reader users can also benefit from headings. Screen reader users can navigate a page according to its headings, listen to a list of all headings, and skip to a desired heading to begin reading at that point. Screen reader users can use headings to skip the repeated blocks of content like headers, menus, and sidebars, for example.

In 2017, WebAIM asked how screen reader users preferred to find information on lengthy web pages (link is external). Almost 70% of respondents said they preferred to use headings on a page. Clearly, organizing pages using headings is one of the best accessibility strategies available.
____
- **What are skip links?**
  - Skip links are internal page links which aid navigation around the current page, rather than to completely new pages. They are mainly used by screen reader users for bypassing or 'skipping' over repetitive web page content. Skip links are not usually visible on a web page for sighted users as these users can easily skip over any information such as navigation to the specific content that interests them. Screen reader users, however, can not easily do so unless provided with a specific link to the main content. This article outlines what skip links are, what the issues are with current approaches, and a best-practice method of implementing them.
    - How can users benefit from them?
      - Skip links are an extremely useful feature of any web page. They are simple to implement and assist users with both physical and visual disabilities. With a CSS layout specifically ordering the HTML source code to display content first will benefit users without CSS enabled devices and screen readers. This reduces the need for unnecessary 'skip to content' links as well as allowing users to access the main content of your web pages immediately.

___
- **What are some of the tools available to test the accessibility of a website or web application?**
Here is a resource [Top 25 Awesome Accessibility Testing Tools for Websites](https://dynomapper.com/blog/27-accessibility-testing/246-top-25-awesome-accessibility-testing-tools-for-websites)
___
- **Describe appropriate instances to use a link, vs a generic button, vs a submit button.**
Here is a resource: [Links, Buttons, & Other Clickable Elements](http://web-accessibility.carnegiemuseums.org/content/buttons/)
