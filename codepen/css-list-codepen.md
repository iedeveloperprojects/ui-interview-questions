# CSS Answer for List Codepen
https://codepen.io/comcastbsee/pen/gQENKM
```
ul {
  border: solid black 1px;
}

li:first-of-type {
  font-weight: bold;
}

div > ul > li:first-of-type {
  color: red;
}

div > ul > li li {
  color: black;
}

body > p + ul li:first-of-type {
  text-transform: uppercase;
}
```